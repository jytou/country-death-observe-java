-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 192.168.0.58:3309
-- Generation Time: Nov 14, 2020 at 03:57 PM
-- Server version: 10.1.44-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `obsfr2`
--

-- --------------------------------------------------------

--
-- Table structure for table `agecol`
--

CREATE TABLE `agecol` (
  `age` int(11) NOT NULL,
  `color` varchar(3) NOT NULL,
  `label` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='All age ranges we want to consider' ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `agecol`
--

INSERT INTO `agecol` (`age`, `color`, `label`) VALUES
(0, 'f00', '0-9'),
(1, '0f0', '10-19'),
(2, '00f', '20-29'),
(3, 'f80', '30-39'),
(4, '409', '40-49'),
(5, 'f0f', '50-59'),
(6, '888', '60-69'),
(7, 'f66', '70-79'),
(8, '077', '80-89'),
(9, '88f', '90+');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agecol`
--
ALTER TABLE `agecol`
  ADD PRIMARY KEY (`age`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
