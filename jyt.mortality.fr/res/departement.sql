-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 192.168.0.58:3309
-- Generation Time: Nov 14, 2020 at 03:57 PM
-- Server version: 10.1.44-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `obsfr2`
--

-- --------------------------------------------------------

--
-- Table structure for table `departement`
--

CREATE TABLE `departement` (
  `id_departement` char(5) NOT NULL DEFAULT '',
  `departement` varchar(50) NOT NULL DEFAULT '',
  `id_region` tinyint(4) DEFAULT NULL,
  `poly` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='All departments with their codes and map regions' ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `departement`
--

INSERT INTO `departement` (`id_departement`, `departement`, `id_region`, `poly`) VALUES
('01000', 'Ain', 22, '384,266,390,263,398,266,407,256,412,258,405,271,398,274,399,284,392,301,381,286,375,293,367,291,360,286,358,278,358,270,363,253,372,253,378,257'),
('02000', 'Aisne', 19, '295,61,292,77,293,90,288,98,289,105,294,112,297,119,304,123,312,115,311,111,314,106,311,100,321,93,325,95,325,88,326,81,330,76,331,61,312,57'),
('03000', 'Allier', 3, '319,268,328,264,326,256,318,253,310,242,306,246,298,244,288,238,273,252,259,257,273,275,287,270,293,277,308,280,318,283'),
('04000', 'Alpes-de-Haute-Provence', 21, '447,352,437,359,435,366,424,362,420,367,413,366,407,373,407,381,400,381,391,385,393,393,394,401,402,406,407,405,412,408,420,402,425,405,433,400,439,400,439,396,447,393,438,383,439,373,446,369,445,361'),
('05000', 'Hautes-Alpes', 21, '433,328,419,331,424,343,411,345,400,355,394,365,388,366,389,371,395,374,397,380,406,378,408,368,414,364,419,367,424,361,435,364,437,358,445,352,451,348,448,341,440,337'),
('06000', 'Alpes-Maritimes', 21, '448,370,439,376,441,385,447,393,437,400,443,406,444,411,449,411,449,417,456,410,463,403,472,399,476,388,479,382,476,376,468,378,455,375'),
('07000', 'Ardèche', 22, '358,320,349,324,336,345,324,351,331,371,340,376,354,376,359,351,363,338'),
('08000', 'Ardennes', 8, '353,50,348,58,341,61,332,61,332,75,327,82,327,93,330,92,339,99,345,99,346,101,359,101,362,93,363,80,367,82,375,80,370,75,361,69,356,69,353,54'),
('09000', 'Ariège', 16, '240,473,255,467,244,460,249,455,249,444,236,435,225,437,219,442,211,444,204,459'),
('10000', 'Aube', 8, '306,137,305,150,309,156,311,162,316,160,320,174,335,174,344,172,352,167,357,163,357,153,348,144,352,139,344,141,338,138,337,132,328,133,319,142'),
('11000', 'Aude', 13, '292,457,291,444,300,438,285,431,280,436,273,427,246,426,239,436,249,444,251,456,245,459,254,467,265,460'),
('12000', 'Aveyron', 16, '305,394,279,344,270,358,253,362,245,374,247,386,256,383,279,407,292,408'),
('13000', 'Bouches-du-Rhône', 21, '362,396,357,409,349,413,342,422,354,423,363,426,370,424,374,428,382,430,389,432,399,435,401,412,384,408,375,406'),
('14000', 'Calvados', 4, '188,91,175,98,165,96,153,94,146,91,138,93,137,98,145,105,145,114,136,123,140,126,147,127,152,124,162,123,173,124,184,119,192,118'),
('15000', 'Cantal', 3, '303,339,301,329,294,321,271,314,254,338,256,348,257,358,260,357,269,358,278,341,287,352'),
('16000', 'Charente', 20, '174,326,210,284,203,277,181,277,170,282,168,293,155,295,161,308,160,317'),
('17000', 'Charente-Maritime', 20, '167,293,167,283,145,266,130,265,126,293,144,316,158,328,168,330,172,325,160,318,162,308,155,294'),
('18000', 'Cher', 7, '289,237,290,215,284,211,285,199,259,192,257,209,248,214,244,220,251,220,256,231,259,258,263,254,271,253,274,246,281,240'),
('19000', 'Corrèze', 14, '268,315,269,299,262,299,252,297,221,314,222,319,221,328,226,330,227,337,236,335,241,341,253,340,260,326'),
('20000', 'Corse', 9, NULL),
('21000', 'Côte-d\'Or', 5, '363,188,360,182,360,178,353,169,345,172,337,174,338,182,333,196,331,208,334,216,353,231,370,228,375,228,375,220,383,209,377,198,382,190,373,192'),
('22000', 'Côtes-d\'Armor', 6, '82,158,91,158,103,151,106,139,97,133,87,130,78,135,61,116,44,124,46,152,55,156,63,154,76,163'),
('23000', 'Creuse', 14, '265,293,271,283,268,267,259,260,230,263,226,271,237,291,248,296,262,300'),
('24000', 'Dordogne', 2, '227,345,228,332,219,329,219,319,223,313,200,297,173,328,171,346,182,346,184,351,189,356,211,360'),
('25000', 'Doubs', 10, '428,192,422,195,415,197,403,205,391,210,396,216,394,223,401,225,403,231,411,235,408,242,407,247,409,249,420,235,420,225,425,223,438,207,441,201,437,202,436,197'),
('26000', 'Drôme', 22, '377,331,376,324,368,318,360,321,364,339,360,350,358,374,368,370,377,376,389,383,397,382,397,375,387,371,388,364,396,362,397,355,389,347,388,333'),
('27000', 'Eure', 11, '188,92,192,121,199,122,206,133,218,129,227,129,232,121,230,113,237,109,241,105,241,94,229,93,217,100,199,90'),
('28000', 'Eure-et-Loir', 7, '236,136,233,120,228,128,207,134,213,141,213,149,205,153,208,162,223,173,234,172,238,167,244,168,251,157,245,145'),
('29000', 'Finistère', 6, '48,168,38,155,45,152,43,125,28,122,17,124,1,131,2,140,17,152,1,157,12,164,14,171,24,169,45,176'),
('2A000', 'Corse-du-Sud', 9, '454,439,458,444,452,445,457,451,461,456,456,461,463,463,460,471,468,475,466,480,481,489,486,481,489,476,489,464,483,465,480,456,470,445'),
('2B000', 'Haute-Corse', 9, '484,401,481,402,482,417,473,416,473,424,452,437,465,440,473,446,480,456,481,465,489,464,491,448,490,425,487,414'),
('30000', 'Gard', 13, '336,416,342,421,360,395,355,377,341,378,329,369,328,384,303,387,305,396,311,402,320,395,334,405'),
('31000', 'Haute-Garonne', 16, '238,436,245,426,253,424,238,414,232,398,222,406,208,405,216,417,208,427,198,427,189,442,194,448,191,455,187,455,187,463,192,463,197,459,204,458,209,444,217,443,225,435'),
('32000', 'Gers', 16, '195,431,202,426,208,427,216,417,196,391,164,398,161,414,176,426'),
('33000', 'Gironde', 2, '176,357,181,351,181,346,171,347,171,330,161,332,129,305,121,365,144,367,154,381,160,380,162,378,165,381'),
('34000', 'Hérault', 13, '299,438,338,413,318,395,310,402,306,397,276,417,273,427,281,436,286,430'),
('35000', 'Ille-et-Vilaine', 6, '136,154,137,140,130,139,123,142,119,133,97,132,106,139,105,149,93,158,98,185,112,185,122,176,128,180,133,169,137,168'),
('36000', 'Indre', 7, '258,257,252,222,243,220,247,215,229,215,223,225,218,226,208,247,220,260,241,258'),
('37000', 'Indre-et-Loire', 7, '217,226,223,225,227,217,217,210,214,195,200,189,186,195,178,216,186,222,187,229,198,225,210,241'),
('38000', 'Isère', 22, '407,309,403,313,381,288,358,311,360,320,367,318,375,325,376,332,387,333,388,346,400,353,410,343,423,342,417,332,414,321,414,314'),
('39000', 'Jura', 10, '383,210,375,223,379,231,382,234,379,237,382,242,380,251,377,255,384,264,391,261,394,264,399,263,409,250,408,240,409,235,400,224,393,223,394,216,390,210'),
('40000', 'Landes', 2, '159,415,162,396,175,392,175,386,155,381,145,368,120,365,113,398,108,416'),
('41000', 'Loir-et-Cher', 7, '256,211,257,204,257,192,245,191,236,185,233,171,224,173,209,163,207,178,201,188,214,193,217,209,225,216,234,213,246,215'),
('42000', 'Loire', 22, '343,300,342,288,338,281,340,272,323,267,321,281,316,291,324,302,328,310,328,318,336,316,343,320,348,325,359,319,358,310'),
('43000', 'Haute-Loire', 3, '331,344,337,344,348,326,339,316,315,318,305,316,295,321,304,337,307,346,315,345,322,350'),
('44000', 'Loire-Atlantique', 18, '127,206,138,204,129,181,122,178,112,185,96,187,97,193,84,198,85,206,93,213,98,220,114,230,121,227,128,221'),
('45000', 'Loiret', 7, '284,185,288,177,293,169,287,163,270,165,266,156,250,156,248,165,239,167,234,172,235,184,243,191,260,192,283,199,288,195'),
('46000', 'Lot', 16, '254,358,251,338,240,341,228,334,227,344,210,370,220,379,243,374'),
('47000', 'Lot-et-Garonne', 2, '205,384,205,373,209,362,182,351,164,383,174,385,174,393,195,390'),
('48000', 'Lozère', 13, '303,384,328,385,328,369,321,352,308,349,304,341,287,355'),
('49000', 'Maine-et-Loire', 18, '176,217,184,196,159,182,131,183,140,203,126,207,130,215,127,221,138,224,153,223'),
('50000', 'Manche', 4, '137,92,135,86,133,79,135,76,129,73,122,76,109,71,111,80,113,88,119,100,120,132,123,141,132,139,145,141,149,135,146,127,139,126,137,122,144,114,143,104'),
('51000', 'Marne', 8, '326,95,322,93,311,100,313,107,312,112,312,116,305,125,306,136,311,141,320,142,327,133,337,132,337,137,342,141,353,138,361,131,357,125,360,116,358,102,336,98'),
('52000', 'Haute-Marne', 8, '361,133,350,143,357,151,357,165,352,169,363,179,362,186,375,192,381,188,388,186,389,179,395,173,386,164,388,157,381,150,372,139'),
('53000', 'Mayenne', 18, '160,180,174,149,167,140,154,145,139,141,139,169,131,182,149,183'),
('54000', 'Meurthe-et-Moselle', 15, '396,108,398,96,395,86,388,82,379,84,380,89,386,91,387,101,392,109,391,117,389,124,392,141,398,147,422,142,428,144,440,137,411,122'),
('55000', 'Meuse', 15, '376,81,363,83,363,97,360,103,360,119,357,124,362,129,362,134,381,146,392,142,388,124,392,114,392,108,386,100,386,90,379,90'),
('56000', 'Morbihan', 6, '95,185,92,159,81,158,78,162,63,154,55,156,44,153,39,155,47,167,45,174,58,186,82,196,96,194'),
('57000', 'Moselle', 15, '397,87,398,98,397,107,401,115,409,117,419,126,440,136,446,123,438,120,432,117,437,110,443,111,450,112,454,114,457,106,450,99,436,102,431,98,426,101,419,94,415,87,403,84'),
('58000', 'Nièvre', 5, '306,199,298,201,287,196,285,208,292,224,291,238,301,244,310,240,320,242,331,235,328,222,334,217,330,209'),
('59000', 'Nord', 17, '255,3,263,17,274,25,282,24,285,32,291,43,289,54,301,56,313,56,323,59,329,59,330,53,326,49,330,43,324,40,312,38,310,33,306,29,302,31,292,15,287,15,284,19,274,13,270,1,262,1'),
('60000', 'Oise', 19, '244,76,241,82,244,89,242,93,243,105,249,107,254,104,263,107,275,111,291,110,288,103,288,98,293,92,289,77,275,83,259,77'),
('61000', 'Orne', 4, '194,123,188,118,173,125,156,124,146,128,149,137,144,143,169,140,175,149,186,143,192,146,192,152,205,159,205,153,211,150,211,142,205,130'),
('62000', 'Pas-de-Calais', 17, '255,5,238,10,238,16,237,30,236,39,237,42,243,39,255,48,263,47,269,49,267,54,273,54,276,49,280,58,288,57,291,48,286,36,280,27'),
('63000', 'Puy-de-Dôme', 3, '316,294,316,283,293,277,284,270,273,276,273,285,266,294,271,303,271,313,283,317,291,321,303,316,325,317,326,306'),
('64000', 'Pyrénées-Atlantiques', 2, '153,458,167,430,162,417,107,418,97,424,113,432,110,441'),
('65000', 'Hautes-Pyrénées', 16, '186,465,188,455,193,450,187,441,193,432,166,419,168,432,155,457,166,464'),
('66000', 'Pyrénées-Orientales', 13, '295,478,293,458,267,461,240,475,250,483,263,479,274,485'),
('67000', 'Bas-Rhin', 1, '458,105,454,114,446,113,438,111,433,116,446,121,446,128,446,134,441,139,443,148,458,159,463,145,466,129,474,118,480,109,474,109,468,106'),
('68000', 'Haut-Rhin', 1, '445,148,441,161,435,168,435,177,442,181,442,189,446,195,454,196,457,190,463,186,458,180,461,162,458,160,450,154'),
('69000', 'Rhône', 22, '357,285,356,273,354,265,345,267,343,273,339,279,342,291,343,301,358,311,371,296,363,289'),
('70000', 'Haute-Saône', 10, '418,173,413,170,410,172,403,167,398,172,390,180,388,187,382,190,380,198,382,209,390,210,399,206,405,204,411,201,416,195,423,195,426,190,426,181,430,178,424,172'),
('71000', 'Saône-et-Loire', 5, '353,232,334,218,328,223,331,234,323,242,314,241,317,252,327,257,328,263,325,267,341,271,341,264,353,263,358,270,363,253,373,252,377,255,381,244,379,235,374,227'),
('72000', 'Sarthe', 18, '199,187,206,180,207,163,193,152,187,144,174,151,159,180,171,191,183,195'),
('73000', 'Savoie', 22, '418,297,409,293,399,285,393,300,397,310,403,313,406,308,411,311,415,316,415,326,426,330,432,327,441,325,452,317,452,309,446,305,445,298,436,292,426,286'),
('74000', 'Haute-Savoie', 22, '416,265,410,272,404,272,399,275,400,284,406,293,411,292,417,297,426,284,432,290,437,290,438,286,447,280,436,269,435,260,436,254,425,255,417,259'),
('75000', 'Paris', 12, '0, 0'),
('76000', 'Seine-Maritime', 11, '230,57,214,65,205,66,185,77,181,85,190,90,196,88,211,95,215,101,225,96,229,91,241,93,241,85,243,75,242,68'),
('77000', 'Seine-et-Marne', 12, '274,115,273,131,271,146,266,153,272,162,284,161,289,152,300,148,305,138,303,125,290,110'),
('78000', 'Yvelines', 12, '235,110,258,119,258,128,246,143,236,133,231,116'),
('79000', 'Deux-Sèvres', 20, '174,248,171,222,154,223,142,226,151,259,147,266,157,274,169,282,182,275,175,260'),
('80000', 'Somme', 19, '236,43,232,55,235,61,241,64,245,75,254,78,260,76,276,83,290,76,295,60,284,58,274,53,267,54,266,48,257,49,243,41'),
('81000', 'Tarn', 16, '251,424,274,424,276,415,290,410,278,408,255,385,238,389,231,399,238,413'),
('82000', 'Tarn-et-Garonne', 16, '219,406,246,386,243,374,226,380,210,373,198,391,206,403'),
('83000', 'Var', 21, '450,418,445,410,437,400,427,405,420,401,414,408,401,405,401,423,399,434,408,442,418,442,426,438,440,433,443,424'),
('84000', 'Vaucluse', 21, '390,386,368,371,356,376,358,387,364,392,359,396,370,401,377,407,384,407,394,410,401,410,402,405,392,396'),
('85000', 'Vendée', 18, '149,256,148,239,141,226,128,222,114,230,99,222,93,229,104,250,128,265,146,264'),
('86000', 'Vienne', 20, '188,229,179,220,172,222,174,256,182,276,203,276,206,266,220,261,208,248,209,242,197,228'),
('87000', 'Haute-Vienne', 14, '234,287,226,271,229,263,213,264,204,277,210,283,200,297,209,303,223,312,247,297'),
('88000', 'Vosges', 15, '389,157,387,163,394,171,404,166,407,171,414,169,417,172,421,169,433,176,434,166,443,148,440,148,439,137,430,144,414,143,399,147,392,142,382,147'),
('89000', 'Yonne', 5, '320,173,315,162,309,161,308,154,303,150,293,152,288,161,292,170,289,182,283,187,287,195,296,200,305,199,313,202,330,209,331,199,338,182,336,175'),
('90000', 'Territoire de Belfort', 10, '431,178,427,192,433,192,438,199,445,192,441,181'),
('91000', 'Essonne', 12, '258,129,271,133,266,152,254,153,249,143'),
('92000', 'Hauts-de-Seine', 12, '0, 0'),
('93000', 'Seine-Saint-Denis', 12, '0, 0'),
('94000', 'Val-de-Marne', 12, '0, 0'),
('95000', 'Val-d\'Oise', 12, '249,107,255,105,273,111,268,117,258,117,236,108'),
('99000', 'Autres', 23, NULL),
('99101', 'DANEMARK', NULL, NULL),
('99102', 'ISLANDE', NULL, NULL),
('99103', 'NORVEGE', NULL, NULL),
('99104', 'SUEDE', NULL, NULL),
('99105', 'FINLANDE', NULL, NULL),
('99106', 'ESTONIE', NULL, NULL),
('99107', 'LETTONIE', NULL, NULL),
('99108', 'LITUANIE', NULL, NULL),
('99109', 'ALLEMAGNE', NULL, NULL),
('99110', 'AUTRICHE', NULL, NULL),
('99111', 'BULGARIE', NULL, NULL),
('99112', 'HONGRIE', NULL, NULL),
('99113', 'LIECHTENSTEIN', NULL, NULL),
('99114', 'ROUMANIE', NULL, NULL),
('99115', 'TCHECOSLOVAQUIE', NULL, NULL),
('99116', 'TCHEQUE (REPUBLIQUE)', NULL, NULL),
('99117', 'SLOVAQUIE', NULL, NULL),
('99118', 'BOSNIE-HERZEGOVINE', NULL, NULL),
('99119', 'CROATIE', NULL, NULL),
('99120', 'MONTENEGRO', NULL, NULL),
('99121', 'SERBIE', NULL, NULL),
('99122', 'POLOGNE', NULL, NULL),
('99123', 'RUSSIE', NULL, NULL),
('99124', 'TURQUIE D\'EUROPE', NULL, NULL),
('99125', 'ALBANIE', NULL, NULL),
('99126', 'GRECE', NULL, NULL),
('99127', 'ITALIE', NULL, NULL),
('99128', 'SAINT-MARIN', NULL, NULL),
('99129', 'VATICAN', NULL, NULL),
('99130', 'ANDORRE', NULL, NULL),
('99131', 'BELGIQUE', NULL, NULL),
('99132', 'ROYAUME-UNI', NULL, NULL),
('99133', 'GIBRALTAR', NULL, NULL),
('99134', 'ESPAGNE', NULL, NULL),
('99135', 'PAYS-BAS', NULL, NULL),
('99136', 'IRLANDE', NULL, NULL),
('99137', 'LUXEMBOURG', NULL, NULL),
('99138', 'MONACO', NULL, NULL),
('99139', 'PORTUGAL', NULL, NULL),
('99140', 'SUISSE', NULL, NULL),
('99141', 'REPUBLIQUE DEMOCRATIQUE ALLEMANDE', NULL, NULL),
('99142', 'REPUBLIQUE FEDERALE D\'ALLEMAGNE', NULL, NULL),
('99144', 'MALTE', NULL, NULL),
('99145', 'SLOVENIE', NULL, NULL),
('99148', 'BIELORUSSIE', NULL, NULL),
('99151', 'MOLDAVIE', NULL, NULL),
('99155', 'UKRAINE', NULL, NULL),
('99156', 'MACEDOINE DU NORD', NULL, NULL),
('99157', 'KOSOVO', NULL, NULL),
('99201', 'ARABIE SAOUDITE', NULL, NULL),
('99202', 'YEMEN (REPUBLIQUE ARABE DU)', NULL, NULL),
('99203', 'IRAQ', NULL, NULL),
('99204', 'IRAN', NULL, NULL),
('99205', 'LIBAN', NULL, NULL),
('99206', 'SYRIE', NULL, NULL),
('99207', 'ISRAEL', NULL, NULL),
('99208', 'TURQUIE', NULL, NULL),
('99209', 'SIBERIE', NULL, NULL),
('99210', 'TURKESTAN RUSSE', NULL, NULL),
('99211', 'KAMTCHATKA', NULL, NULL),
('99212', 'AFGHANISTAN', NULL, NULL),
('99213', 'PAKISTAN', NULL, NULL),
('99214', 'BHOUTAN', NULL, NULL),
('99215', 'NEPAL', NULL, NULL),
('99216', 'CHINE', NULL, NULL),
('99217', 'JAPON', NULL, NULL),
('99218', 'MANDCHOURIE', NULL, NULL),
('99219', 'THAILANDE', NULL, NULL),
('99220', 'PHILIPPINES', NULL, NULL),
('99221', 'POSSESSIONS BRITANNIQUES AU PROCHE-ORIENT', NULL, NULL),
('99222', 'JORDANIE', NULL, NULL),
('99223', 'INDE', NULL, NULL),
('99224', 'BIRMANIE', NULL, NULL),
('99225', 'BRUNEI', NULL, NULL),
('99226', 'SINGAPOUR', NULL, NULL),
('99227', 'MALAISIE', NULL, NULL),
('99228', 'ETATS MALAIS NON FEDERES', NULL, NULL),
('99229', 'MALDIVES', NULL, NULL),
('99230', 'HONG-KONG', NULL, NULL),
('99231', 'INDONESIE', NULL, NULL),
('99232', 'MACAO', NULL, NULL),
('99233', 'YEMEN DEMOCRATIQUE', NULL, NULL),
('99234', 'CAMBODGE', NULL, NULL),
('99235', 'SRI LANKA', NULL, NULL),
('99236', 'TAIWAN', NULL, NULL),
('99237', 'COREE', NULL, NULL),
('99238', 'COREE (REPUBLIQUE POPULAIRE DEMOCRATIQUE DE)', NULL, NULL),
('99239', 'COREE (REPUBLIQUE DE)', NULL, NULL),
('99240', 'KOWEIT', NULL, NULL),
('99241', 'LAOS', NULL, NULL),
('99242', 'MONGOLIE', NULL, NULL),
('99243', 'VIET NAM', NULL, NULL),
('99244', 'VIET NAM DU NORD', NULL, NULL),
('99245', 'VIET NAM DU SUD', NULL, NULL),
('99246', 'BANGLADESH', NULL, NULL),
('99247', 'EMIRATS ARABES UNIS', NULL, NULL),
('99248', 'QATAR', NULL, NULL),
('99249', 'BAHREIN', NULL, NULL),
('99250', 'OMAN', NULL, NULL),
('99251', 'YEMEN', NULL, NULL),
('99252', 'ARMENIE', NULL, NULL),
('99253', 'AZERBAIDJAN', NULL, NULL),
('99254', 'CHYPRE', NULL, NULL),
('99255', 'GEORGIE', NULL, NULL),
('99256', 'KAZAKHSTAN', NULL, NULL),
('99257', 'KIRGHIZISTAN', NULL, NULL),
('99258', 'OUZBEKISTAN', NULL, NULL),
('99259', 'TADJIKISTAN', NULL, NULL),
('99260', 'TURKMENISTAN', NULL, NULL),
('99261', 'PALESTINE (Etat de)', NULL, NULL),
('99262', 'TIMOR ORIENTAL', NULL, NULL),
('99301', 'EGYPTE', NULL, NULL),
('99302', 'LIBERIA', NULL, NULL),
('99303', 'AFRIQUE DU SUD', NULL, NULL),
('99304', 'GAMBIE', NULL, NULL),
('99305', 'CAMEROUN ET TOGO', NULL, NULL),
('99306', 'SAINTE HELENE', NULL, NULL),
('99307', 'KENYA OUGANDA', NULL, NULL),
('99308', 'OCEAN INDIEN (TERRITOIRE BRITANNIQUE DE L\')', NULL, NULL),
('99309', 'TANZANIE', NULL, NULL),
('99310', 'ZIMBABWE', NULL, NULL),
('99311', 'NAMIBIE', NULL, NULL),
('99312', 'CONGO (REPUBLIQUE DEMOCRATIQUE)', NULL, NULL),
('99313', 'CANARIES (ILES)', NULL, NULL),
('99314', 'GUINEE EQUATORIALE', NULL, NULL),
('99315', 'ETHIOPIE', NULL, NULL),
('99316', 'LIBYE', NULL, NULL),
('99317', 'ERYTHREE', NULL, NULL),
('99318', 'SOMALIE', NULL, NULL),
('99319', 'ACORES', NULL, NULL),
('99320', 'ILES PORTUGAISES DE L\'OCEAN INDIEN', NULL, NULL),
('99321', 'BURUNDI', NULL, NULL),
('99322', 'CAMEROUN', NULL, NULL),
('99323', 'CENTRAFRICAINE (REPUBLIQUE)', NULL, NULL),
('99324', 'CONGO', NULL, NULL),
('99325', 'TANGER', NULL, NULL),
('99326', 'COTE D\'IVOIRE', NULL, NULL),
('99327', 'BENIN', NULL, NULL),
('99328', 'GABON', NULL, NULL),
('99329', 'GHANA', NULL, NULL),
('99330', 'GUINEE', NULL, NULL),
('99331', 'BURKINA', NULL, NULL),
('99332', 'KENYA', NULL, NULL),
('99333', 'MADAGASCAR', NULL, NULL),
('99334', 'MALAWI', NULL, NULL),
('99335', 'MALI', NULL, NULL),
('99336', 'MAURITANIE', NULL, NULL),
('99337', 'NIGER', NULL, NULL),
('99338', 'NIGERIA', NULL, NULL),
('99339', 'OUGANDA', NULL, NULL),
('99340', 'RWANDA', NULL, NULL),
('99341', 'SENEGAL', NULL, NULL),
('99342', 'SIERRA LEONE', NULL, NULL),
('99343', 'SOUDAN', NULL, NULL),
('99344', 'TCHAD', NULL, NULL),
('99345', 'TOGO', NULL, NULL),
('99346', 'ZAMBIE', NULL, NULL),
('99347', 'BOTSWANA', NULL, NULL),
('99348', 'LESOTHO', NULL, NULL),
('99349', 'SOUDAN DU SUD', NULL, NULL),
('99350', 'MAROC', NULL, NULL),
('99351', 'TUNISIE', NULL, NULL),
('99352', 'ALGERIE', NULL, NULL),
('99389', 'SAHARA OCCIDENTAL', NULL, NULL),
('99390', 'MAURICE', NULL, NULL),
('99391', 'ESWATINI', NULL, NULL),
('99392', 'GUINEE-BISSAU', NULL, NULL),
('99393', 'MOZAMBIQUE', NULL, NULL),
('99394', 'SAO TOME-ET-PRINCIPE', NULL, NULL),
('99395', 'ANGOLA', NULL, NULL),
('99396', 'CAP-VERT', NULL, NULL),
('99397', 'COMORES', NULL, NULL),
('99398', 'SEYCHELLES', NULL, NULL),
('99399', 'DJIBOUTI', NULL, NULL),
('99401', 'CANADA', NULL, NULL),
('99402', 'TERRE-NEUVE', NULL, NULL),
('99403', 'LABRADOR', NULL, NULL),
('99404', 'ETATS-UNIS', NULL, NULL),
('99405', 'MEXIQUE', NULL, NULL),
('99406', 'COSTA RICA', NULL, NULL),
('99407', 'CUBA', NULL, NULL),
('99408', 'DOMINICAINE (REPUBLIQUE)', NULL, NULL),
('99409', 'GUATEMALA', NULL, NULL),
('99410', 'HAITI', NULL, NULL),
('99411', 'HONDURAS', NULL, NULL),
('99412', 'NICARAGUA', NULL, NULL),
('99413', 'PANAMA', NULL, NULL),
('99414', 'EL SALVADOR', NULL, NULL),
('99415', 'ARGENTINE', NULL, NULL),
('99416', 'BRESIL', NULL, NULL),
('99417', 'CHILI', NULL, NULL),
('99418', 'BOLIVIE', NULL, NULL),
('99419', 'COLOMBIE', NULL, NULL),
('99420', 'EQUATEUR', NULL, NULL),
('99421', 'PARAGUAY', NULL, NULL),
('99422', 'PEROU', NULL, NULL),
('99423', 'URUGUAY', NULL, NULL),
('99424', 'VENEZUELA', NULL, NULL),
('99425', 'TERRITOIRES DU ROYAUME-UNI AUX ANTILLES', NULL, NULL),
('99426', 'JAMAIQUE', NULL, NULL),
('99427', 'MALOUINES/FALKLAND', NULL, NULL),
('99428', 'GUYANA', NULL, NULL),
('99429', 'BELIZE', NULL, NULL),
('99430', 'GROENLAND', NULL, NULL),
('99431', 'ANTILLES NEERLANDAISES', NULL, NULL),
('99432', 'VIERGES DES ETATS-UNIS (ILES)', NULL, NULL),
('99433', 'TRINITE-ET-TOBAGO', NULL, NULL),
('99434', 'BARBADE', NULL, NULL),
('99435', 'GRENADE', NULL, NULL),
('99436', 'BAHAMAS', NULL, NULL),
('99437', 'SURINAME', NULL, NULL),
('99438', 'DOMINIQUE', NULL, NULL),
('99439', 'SAINTE-LUCIE	', NULL, NULL),
('99440', 'SAINT-VINCENT-ET-LES GRENADINES', NULL, NULL),
('99441', 'ANTIGUA-ET-BARBUDA', NULL, NULL),
('99442', 'SAINT-CHRISTOPHE-ET-NIEVES', NULL, NULL),
('99443', 'BONAIRE', NULL, NULL),
('99444', 'CURAÇAO', NULL, NULL),
('99445', 'SAINT-MARTIN (PARTIE NEERLANDAISE)', NULL, NULL),
('99501', 'AUSTRALIE', NULL, NULL),
('99502', 'NOUVELLE-ZELANDE', NULL, NULL),
('99503', 'PITCAIRN (ILE)', NULL, NULL),
('99504', 'HAWAII (ILES)', NULL, NULL),
('99505', 'TERR. DES ETATS-UNIS D\'AMERIQUE EN OCEANIE', NULL, NULL),
('99506', 'SAMOA OCCIDENTALES', NULL, NULL),
('99507', 'NAURU', NULL, NULL),
('99508', 'FIDJI', NULL, NULL),
('99509', 'TONGA', NULL, NULL),
('99510', 'PAPOUASIE-NOUVELLE-GUINEE', NULL, NULL),
('99511', 'TUVALU', NULL, NULL),
('99512', 'SALOMON (ILES)', NULL, NULL),
('99513', 'KIRIBATI', NULL, NULL),
('99514', 'VANUATU', NULL, NULL),
('99515', 'MARSHALL (ILES)', NULL, NULL),
('99516', 'MICRONESIE (ETATS FEDERES DE)', NULL, NULL),
('99517', 'PALAOS (ILES)', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`id_departement`),
  ADD KEY `id_region` (`id_region`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
