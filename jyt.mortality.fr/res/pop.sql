-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 192.168.0.58:3309
-- Generation Time: Nov 14, 2020 at 03:58 PM
-- Server version: 10.1.44-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `obsfr2`
--

-- --------------------------------------------------------

--
-- Table structure for table `pop`
--

CREATE TABLE `pop` (
  `yyyy` varchar(4) NOT NULL,
  `pop` int(11) NOT NULL,
  `color` varchar(3) NOT NULL,
  `col2` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Population of the country for every year, in order to calculate mortality per million inhabitants';

--
-- Dumping data for table `pop`
--

INSERT INTO `pop` (`yyyy`, `pop`, `color`, `col2`) VALUES
('2000', 60508150, '000', '000'),
('2001', 60941410, '009', '009'),
('2002', 61385070, '00f', '00f'),
('2003', 61824030, '090', 'f00'),
('2004', 62251062, '099', '099'),
('2005', 62730537, '09f', '09f'),
('2006', 63186117, '0f0', '0f0'),
('2007', 63186117, '0f9', '0f9'),
('2008', 63961859, '0ff', 'f09'),
('2009', 64304500, '900', '900'),
('2010', 64612939, '909', '909'),
('2011', 64933400, '90f', '90f'),
('2012', 65241241, '990', '990'),
('2013', 65564756, '999', '999'),
('2014', 66130873, 'f00', '090'),
('2015', 66422469, 'f09', '0ff'),
('2016', 66602645, 'f0f', 'f0f'),
('2017', 66774482, 'f90', 'f90'),
('2018', 66883761, 'f99', 'f99'),
('2019', 66977703, 'ff0', '5a0'),
('2020', 67063703, 'ff9', '05a');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pop`
--
ALTER TABLE `pop`
  ADD PRIMARY KEY (`yyyy`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
