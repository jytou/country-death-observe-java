-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 192.168.0.58:3309
-- Generation Time: Nov 14, 2020 at 03:59 PM
-- Server version: 10.1.44-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `obsfr2`
--

-- --------------------------------------------------------

--
-- Table structure for table `obs`
--

CREATE TABLE `obs` (
  `nom` varchar(80) NOT NULL,
  `sexe` tinyint(4) NOT NULL,
  `yyyynais` varchar(4) NOT NULL,
  `mmnais` varchar(2) NOT NULL,
  `ddnais` varchar(2) NOT NULL,
  `codenais` varchar(5) NOT NULL,
  `comnais` varchar(30) NOT NULL,
  `paysnais` varchar(30) NOT NULL,
  `yyyydec` varchar(4) NOT NULL,
  `mmdec` varchar(2) NOT NULL,
  `dddec` varchar(2) NOT NULL,
  `dptdec` varchar(2) NOT NULL,
  `com2dec` varchar(3) NOT NULL,
  `nactedec` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Contains direct imports of raw data';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `obs`
--
ALTER TABLE `obs`
  ADD UNIQUE KEY `unik` (`nom`,`sexe`,`yyyynais`,`mmnais`,`ddnais`,`codenais`,`comnais`,`paysnais`) USING BTREE,
  ADD KEY `sexe` (`sexe`),
  ADD KEY `nactedec` (`nactedec`),
  ADD KEY `yyyynais` (`yyyynais`,`mmnais`,`ddnais`),
  ADD KEY `yyyydec` (`yyyydec`,`mmdec`,`dddec`),
  ADD KEY `dptdec` (`dptdec`),
  ADD KEY `com2dec` (`com2dec`),
  ADD KEY `yyyydec_2` (`yyyydec`),
  ADD KEY `mmdec` (`mmdec`),
  ADD KEY `jjdec` (`dddec`),
  ADD KEY `nom` (`nom`),
  ADD KEY `yyyynais_2` (`yyyynais`),
  ADD KEY `mmnais` (`mmnais`),
  ADD KEY `ddnais` (`ddnais`),
  ADD KEY `codenais` (`codenais`),
  ADD KEY `comnais` (`comnais`),
  ADD KEY `paysnais` (`paysnais`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
