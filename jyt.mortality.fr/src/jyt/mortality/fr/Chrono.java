package jyt.mortality.fr;

public class Chrono
{
	private long mCurrent;

	public Chrono()
	{
		super();
		start();
	}

	public void start()
	{
		mCurrent = System.currentTimeMillis();
	}

	public String getStringAndRestart()
	{
		final long rez = System.currentTimeMillis() - mCurrent;
		mCurrent = System.currentTimeMillis();
		if (rez < 10 * 1000)
			return "" + rez + " ms";
		else if (rez < 5 * 60 * 1000)
			return "" + (rez / 1000) + " s";
		else if (rez < 120 * 60 * 1000)
			return "" + (rez / 60 / 1000) + " m";
		else
			return "" + (rez / 3600000) + " h";
	}
}
