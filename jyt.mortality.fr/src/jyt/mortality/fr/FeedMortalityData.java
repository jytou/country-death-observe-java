package jyt.mortality.fr;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class FeedMortalityData
{
	// number of inserted row per JDBC batch
	private static final int PER_BATCH = 5000;
	// column definition of the source file
	private static final int[] LIMITS = {80, 1, 4, 2, 2, 5, 30, 30, 4, 2, 2, 2, 3, 9};
	// column definitions of the target table
	private static final String[] COLUMNS = {"nom", "sexe", "yyyynais", "mmnais", "ddnais", "codenais", "comnais", "paysnais", "yyyydec", "mmdec", "dddec", "dptdec", "com2dec", "nactedec"};

	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException
	{
		final String dburl = args[0];
		final String dbuser = args[1];
		final String dbpass = args[2];
		final String srcfile = args[3];
		final boolean filterDuplicates = args.length == 4;

		final BufferedReader r = new BufferedReader(new FileReader(srcfile));

		final Connection conn = DriverManager.getConnection(dburl, dbuser, dbpass);

		// prepare sql insert of all columns defined in COLUMNS
		final StringBuilder sql = new StringBuilder("insert into obs(");
		final StringBuilder extraSql = new StringBuilder();
		for (int i = 0; i < LIMITS.length; i++)
		{
			if (i > 0)
			{
				sql.append(", ");
				extraSql.append(", ");
			}
			sql.append(COLUMNS[i]);
			extraSql.append("?");
		}
		final PreparedStatement insert = conn.prepareStatement(sql.append(") values (").append(extraSql).append(")").toString());

		// if we want to verify duplicates, prepare that statement as well
		PreparedStatement verify = null;
		if (filterDuplicates)
		{
			System.out.println("Will filter duplicates.");
			final StringBuilder sb = new StringBuilder();
			for (int i = 0; i < 8; i++)
			{
				if (sb.length() > 0)
					sb.append(" and ");
				sb.append(COLUMNS[i]).append("=?");
			}
			verify = conn.prepareStatement("select * from obs where " + sb.toString());
		}

		// prepare to read the file
		String l;
		int inBatch = PER_BATCH;
		// because there are some duplicates which are pretty close in the file and we might not have inserted the duplicates yet, we need to keep the unique keys in memory
		final Set<String> currentlyAdding = new HashSet<>();
		while ((l = r.readLine()) != null)
		{
			if (!l.isEmpty())
			{
				final String[] result = new String[LIMITS.length];
				final String[] unik = new String[8];

				// read all columns of this line
				int curIndex = 0;
				for (int i = 0; i < LIMITS.length; i++)
				{
					String str = l.substring(curIndex, curIndex + LIMITS[i]).trim();
					if (i == 0)
						str = str.substring(0, str.length() - 1);
					else if (((i == 10) || (i == 9)) && ("00".equals(str)))
					// because some days and months are written as 00 where they must be 01
						str = "01";

					result[i] = str;
					// also fill the unik array to avoid duplicates
					if (i < 8)
						unik[i] = str;

					curIndex += LIMITS[i];
					insert.setString(i + 1, str);
				}

				System.out.println(Arrays.deepToString(result));
				if (verify != null)
				// we want to verify duplicates
				{
					final String currentToAdd = Arrays.deepToString(unik);
					if (currentlyAdding.contains(currentToAdd))
					// present in memory, not yet inserted but will be inserted
					{
						System.out.println("     => IGNORE DUPLICATE");
						continue;
					}
					currentlyAdding.add(currentToAdd);

					// now verify if it is in the database
					for (int p = 0; p < 8; p++)
						verify.setString(p + 1, result[p]);
					final ResultSet rs = verify.executeQuery();
					try
					{
						if (rs.next())
						{
							System.out.println("     => IGNORE DUPLICATE");
							// We do have a result
							continue;
						}
						// else we don't have a result, go on...
					}
					finally
					{
						rs.close();
					}
				}
				insert.addBatch();
				inBatch--;
				if (inBatch == 0)
				{
					insert.executeBatch();
					currentlyAdding.clear();
					inBatch = PER_BATCH;
				}
			}
		}
		if (inBatch < PER_BATCH)
			insert.executeBatch();
		r.close();
		conn.close();
	}
}
