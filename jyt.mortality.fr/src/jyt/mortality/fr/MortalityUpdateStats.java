package jyt.mortality.fr;

import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

public class MortalityUpdateStats extends JFrame
{
	// number of concurrent threads
	private static final int NB_THREADS = 10;
	// days to take into account in the smoothing of data
	private static final int NB_SMOOTH = 9;

	// do not take into account data later than this date, since it will be totally incomplete and inaccurate
	//private static final String UP_TO_DATE = "2020-07-15";
	private static String sUpToDate;// now updated automatically

	private static Set<String> sAllTables = new HashSet<>();
	private static Map<String, Integer> sPopYear = new HashMap<>();
	private static Map<String, Integer> sPopYear70 = new HashMap<>();

	public static class UpdateInfo
	{
		String dpt;
		int code;
		public UpdateInfo(String pDpt, int pCode)
		{
			super();
			dpt = pDpt;
			code = pCode;
		}
	}
	private static LinkedBlockingQueue<UpdateInfo> sUpdateQueue = new LinkedBlockingQueue<>();
	public static class ThreadStatus
	{
		double progress = -1;
		UpdateInfo info;
		String status = "Waiting...";
		int s = 0;// 0=running, -1=initializing, -2=done
	}
	private static List<ThreadStatus> sThreadStatuses = new ArrayList<>();
	private static List<JProgressBar> sProgressBars = new ArrayList<>();
	private static List<JLabel> sLabels = new ArrayList<>();
	private static String sDbUrl;
	private static String sDbUser;
	private static String sDbPass;

	public MortalityUpdateStats()
	{
		super("Mortality Update Stats");
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				super.windowClosing(e);
				System.exit(0);
			}
		});
		setSize(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getBounds().getSize());
		setExtendedState(Frame.MAXIMIZED_BOTH);
		getContentPane().setLayout(new GridBagLayout());
		for (int i = 0; i < sThreadStatuses.size(); i++)
		{
			final JProgressBar progressBar = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
			sProgressBars.add(progressBar);
			getContentPane().add(progressBar, new GridBagConstraints(0, i, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 5, 5));
			final JLabel label = new JLabel("Waiting...");
			sLabels.add(label);
			getContentPane().add(label, new GridBagConstraints(1, i, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 5, 5));
		}
		new Thread("UIUpdater")
		{
			public void run()
			{
				while (true)
				{
					boolean oneRunning = false;
					for (int i = 0; i < sThreadStatuses.size(); i++)
					{
						final ThreadStatus threadStatus = sThreadStatuses.get(i);
						synchronized (threadStatus)
						{
							if (threadStatus.s == -2)
							{
								sProgressBars.get(i).setValue(0);
								sLabels.get(i).setText("Stopped");
							}
							else
							{
								oneRunning = true;
								if (threadStatus.s == -1)
								{
									sProgressBars.get(i).setValue(0);
									sLabels.get(i).setText("Waiting...");
								}
								else
								{
									sProgressBars.get(i).setValue((int)(threadStatus.progress * 100));
									sLabels.get(i).setText(threadStatus.status);
								}
							}
						}
					}
					if (!oneRunning)
						break;
					try
					{
						Thread.sleep(1000);
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
				System.out.println("Done");
				System.out.flush();
				setVisible(false);
				System.exit(0);
			}
		}.start();
		setVisible(true);
	}

	public static class UpdateThread extends Thread
	{
		private int mNum;

		public UpdateThread(int pNum)
		{
			super("Update Thread #" + pNum);
			mNum = pNum;
		}

		@Override
		public void run()
		{
			final ThreadStatus threadStatus = sThreadStatuses.get(mNum);
			try (final Connection conn = getConnection())
			{
				while (true)
				{
					synchronized (threadStatus)
					{
						threadStatus.status = "Waiting...";
						threadStatus.s = -1;
					}
					final UpdateInfo updateInfo = sUpdateQueue.take();
					if (updateInfo.dpt == null)
						break;
					else
						recomputeStats(mNum, conn, updateInfo);
				}
			}
			catch (SQLException | InterruptedException e)
			{
				e.printStackTrace();
			}
			finally
			{
				synchronized (threadStatus)
				{
					threadStatus.s = -2;
				}
			}
		}
	}

	public static void main(String[] args) throws SQLException, InterruptedException
	{
		sDbUrl = args[0];
		final String schema = sDbUrl.substring(sDbUrl.lastIndexOf('/') + 1);
		sDbUser = args[1];
		sDbPass = args[2];
		for (int i = 0; i < MortalityUpdateStats.NB_THREADS; i++)
			sThreadStatuses.add(new ThreadStatus());
		for (int i = 0; i < sThreadStatuses.size(); i++)
			new UpdateThread(i).start();
		try (final Connection conn = getConnection())
		{
			final Chrono chrono = new Chrono();
			System.out.print("Getting max year...");
			final Statement s = conn.createStatement();
			ResultSet rs = s.executeQuery("select max(yyyydec) from obs");
			String maxYear;
			if (rs.next())
				maxYear = rs.getString(1);
			else
				throw new RuntimeException("Could not get max year");
			rs.close();
			System.out.print(" " + maxYear + "\nGetting max month within that year...");
			rs = s.executeQuery("select max(mmdec) from obs where yyyydec='" + maxYear + "'");
			String maxMonth;
			if (rs.next())
				maxMonth = rs.getString(1);
			else
				throw new RuntimeException("Could not get max month");
			rs.close();
			System.out.print(" " + maxMonth + "\nGetting max day within that year/month...");
			rs = s.executeQuery("select max(dddec) from obs where yyyydec='" + maxYear + "' and mmdec='" + maxMonth + "'");
			String maxDay;
			if (rs.next())
				maxDay = rs.getString(1);
			else
				throw new RuntimeException("Could not get max day");
			rs.close();
			if (Integer.parseInt(maxDay) > 15)
				sUpToDate = maxYear + "-" + maxMonth + "-15";
			else
			{
				int yyyy = Integer.parseInt(maxYear);
				int mm = Integer.parseInt(maxMonth);
				mm--;
				if (mm == 0)
				{
					mm = 12;
					yyyy--;
				}
				sUpToDate = "" + yyyy + "-" + String.format("%02d", mm) + "-15";
			}
			System.out.println(" " + maxDay + "\nWill stop at " + sUpToDate);

			new MortalityUpdateStats();
			System.out.print("Getting populations per year...");
			rs = s.executeQuery("select yyyy, pop, pop70 from pop");
			while (rs.next())
			{
				sPopYear.put(rs.getString("yyyy"), rs.getInt("pop"));
				sPopYear70.put(rs.getString("yyyy"), rs.getInt("pop70"));
			}
			rs.close();
			if (!sPopYear.containsKey(maxYear))
				throw new RuntimeException("Please fill the population census in table pop.\nYou can use the following URL for the current year for France: https://www.insee.fr/fr/statistiques/2381474 \nFor the history, the data is there: https://www.insee.fr/fr/statistiques/6686993?sommaire=6686521&q=bilan+demographique");
			System.out.println(chrono.getStringAndRestart());
			rs = conn.getMetaData().getTables(schema, null, "dstat%", null);
			while (rs.next())
				sAllTables.add(rs.getString("TABLE_NAME"));
			rs.close();
			final SortedSet<String> allDeps = new TreeSet<>();
//			allDeps.add("00");
			rs = s.executeQuery("SELECT distinct dptdec FROM `obs` where dptdec<>'99' order by dptdec");
			allDeps.add("00");
			while (rs.next())
			{
				final String dpt = rs.getString(1);
				if (!dpt.isEmpty())
					allDeps.add(dpt);
			}
			rs.close();

			for (String dpt : allDeps)
				sUpdateQueue.put(new UpdateInfo(dpt, -1));

			final SortedSet<Integer> countryCodes = new TreeSet<>();
			rs = s.executeQuery("select distinct com2dec from obs where dptdec='99'");
			while (rs.next())
				countryCodes.add(Integer.parseInt(rs.getString(1)));
			rs.close();
			s.close();
			for (Integer code : countryCodes)
			{
//				if (code <= 260)
//					continue;
				sUpdateQueue.put(new UpdateInfo("99", code));
			}
			// signal "the end" to all threads
			for (int i = 0; i < sThreadStatuses.size(); i++)
				sUpdateQueue.put(new UpdateInfo(null, -1));
		}
	}

	private static void recomputeStats(final int pThreadNumber, final Connection pConn, final UpdateInfo pUpdateInfo) throws SQLException
	{
		final String fullCode = computeFullCode(pUpdateInfo);
		if (!sAllTables.contains("dstat" + fullCode))
			createTable(pConn, fullCode);
		updateDptInMemory(pThreadNumber, pConn, pUpdateInfo);
	}

	private static Connection getConnection() throws SQLException
	{
		return DriverManager.getConnection(sDbUrl, sDbUser, sDbPass);
	}

	public static class MutableDouble
	{
		double d = 0;
	}

	public static void createTable(final Connection pConn, final String pCode)
	{
		try
		{
			pConn.createStatement().execute("CREATE TABLE `dstat" + pCode + "` (\n" + 
			 		"  `yyyy` varchar(4) NOT NULL,\n" + 
			 		"  `mm` varchar(2) NOT NULL,\n" + 
			 		"  `dd` varchar(2) NOT NULL,\n" + 
			 		"  `code` varchar(5) DEFAULT NULL,\n" + 
			 		"  `age` int(11) DEFAULT NULL COMMENT 'tranche d''âge, pour tous si null',\n" + 
			 		"  `srt` int(11) DEFAULT NULL COMMENT 'sort order',\n" + 
			 		"  `nb` int(11) NOT NULL,\n" + 
			 		"  `nbpond` double DEFAULT NULL,\n" + 
			 		"  `nb10` double DEFAULT NULL,\n" + 
			 		"  `nbpond70` double DEFAULT NULL,\n" + 
			 		"  `nb1070` double DEFAULT NULL\n" + 
			 		")");
			pConn.createStatement().execute("ALTER TABLE `dstat" + pCode + "`\n" + 
					"  ADD KEY `yyyy" + pCode + "` (`yyyy`),\n" + 
					"  ADD KEY `mm" + pCode + "` (`mm`),\n" + 
					"  ADD KEY `dd" + pCode + "` (`dd`),\n" + 
					"  ADD KEY `age" + pCode + "` (`age`),\n" + 
					"  ADD KEY `srt" + pCode + "` (`srt`),\n" + 
					"  ADD KEY `code" + pCode + "` (`code`),\n" + 
					"  ADD KEY `yyyy_2" + pCode + "` (`yyyy`,`mm`,`dd`);");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void updateDptInMemory(final int pThreadNumber, final Connection pConn, UpdateInfo pInfo) throws SQLException
	{
		final ThreadStatus threadInfo = sThreadStatuses.get(pThreadNumber);
		final String prefix = "Department " + pInfo.dpt + " / code " + pInfo.code + " - ";
		synchronized (threadInfo)
		{
			threadInfo.progress = 0;
			threadInfo.s = 0;
			threadInfo.info = pInfo;
			threadInfo.status = prefix + "Starting...";
		}
		// department filter
		final String fullCode = computeFullCode(pInfo);
		final String dptFilter = "00".equals(pInfo.dpt) ? "code is null" : "code='" + fullCode + "'";
		for (int iAge = -1; iAge < 10; iAge++)
//		for (int iAge = -1; iAge < 2; iAge++)
		{
			final String prefixAge = prefix + " Age " + iAge + " - ";
			final SortedMap<String, Integer> stats = new TreeMap<>();
			final String ageFilter = iAge == -1 ? "age is null" : "age=" + iAge;
			final String fullFilter = ageFilter + " and " + dptFilter;
			// cleanup existing data
//			System.out.print("Deleting existing data for dpt " + fullCode + " age " + iAge + "...");
			synchronized (threadInfo)
			{
				threadInfo.status = prefixAge + "Deleting existing data...";
			}
			pConn.createStatement().execute("delete from dstat" + fullCode + " where " + fullFilter);
			// Insert all dates
//			System.out.print(pChrono.getStringAndRestart() + "\nGet all dates for dpt " + fullCode + " age " + iAge + "...");
			synchronized (threadInfo)
			{
				threadInfo.progress += 1.0/11/4;
				threadInfo.status = prefixAge + "Getting all dates...";
			}
			ResultSet rs = pConn.createStatement().executeQuery("select dte from alldates where dte<='" + sUpToDate + "'");
			while (rs.next())
				stats.put(rs.getString(1), 0);
			rs.close();
			// fill data with newest updates
//			System.out.print(pChrono.getStringAndRestart() + "\nCount for dpt " + fullCode + " age " + iAge + "...");
			synchronized (threadInfo)
			{
				threadInfo.progress += 1.0/11/4;
				threadInfo.status = prefixAge + "Counting data...";
			}
			rs = pConn.createStatement().executeQuery("select yyyydec, mmdec, dddec, count(*) from obs where yyyydec>=yyyynais and yyyydec>='2000'" +
			                                ("00".equals(pInfo.dpt) ? "" : " and dptdec='" + pInfo.dpt + "'") +
			                                (pInfo.code == -1 ? "" : " and com2dec='" + String.format("%03d", pInfo.code) + "'") +
			                                (iAge == -1 ? "" : " and yyyydec>=yyyynais and cast(yyyydec as unsigned)-cast(yyyynais as unsigned)>=" + (iAge*10) +
			                                 (iAge == 9 ? "" : " and cast(yyyydec as unsigned)-cast(yyyynais as unsigned)<" + (iAge * 10 + 10))) +
			                                " group by yyyydec, mmdec, dddec");
			long total = 0;
			while (rs.next())
			{
				final String yyyy = rs.getString(1);
				final String dte = yyyy + "-" + rs.getString(2) + "-" + rs.getString(3);
				if (stats.get(dte) != null)
				{
					final int count = rs.getInt(4);
					total += count;
					stats.put(dte, stats.get(dte) + count);
				}
			}
			rs.close();
			// smoothing - interval NB_SMOOTH days
//			System.out.println(pChrono.getStringAndRestart() + "\nInserting for dpt " + fullCode + " age " + iAge + "...");
			synchronized (threadInfo)
			{
				threadInfo.progress += 1.0/11/4;
				threadInfo.status = prefixAge + "Inserting data...";
			}
			final PreparedStatement ps = pConn.prepareStatement("insert into dstat" + fullCode + "(yyyy, mm, dd, code, age, nb, nbpond, nb10, nbpond70, nb1070) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			double[] nbs = new double[NB_SMOOTH];
			final double avg = 1000000.0 * total / stats.size() / sPopYear.get("2000");
			Arrays.fill(nbs, avg);
			double sum = nbs.length * avg;

			double[] nbs70 = new double[NB_SMOOTH];
			final double avg70 = 1000000.0 * total / stats.size() / sPopYear70.get("2000");
			Arrays.fill(nbs70, avg70);
			double sum70 = nbs70.length * avg70;

			int index = 0;
			int nbBatch = 0;
			int nbUpdated = 0;
			for (Map.Entry<String, Integer> entry : stats.entrySet())
			{
				final String dte = entry.getKey();
				final int nb = entry.getValue();
				final String yyyy = dte.substring(0, 4);
				final String mm = dte.substring(5, 7);
				final String dd = dte.substring(8, 10);
				final double pondNb = 1000000.0 * nb / sPopYear.get(yyyy);
				final double pondNb70 = 1000000.0 * nb / sPopYear70.get(yyyy);
				ps.setString(1, yyyy);
				ps.setString(2, mm);
				ps.setString(3, dd);
				if ("00".equals(pInfo.dpt))
					ps.setNull(4, Types.VARCHAR);
				else
					ps.setString(4, fullCode);
				if (iAge == -1)
					ps.setNull(5, Types.NUMERIC);
				else
					ps.setInt(5, iAge);
				ps.setInt(6, nb);
				ps.setDouble(7, pondNb);
				ps.setDouble(8, (sum + pondNb)/(NB_SMOOTH + 1));
				ps.setDouble(9, pondNb70);
				ps.setDouble(10, (sum70 + pondNb70)/(NB_SMOOTH + 1));
				ps.addBatch();
				nbBatch++;
				if (nbBatch > 5000)
				{
					ps.executeBatch();
					nbBatch = 0;
				}
				sum -= nbs[index];
				sum += pondNb;
				nbs[index] = pondNb;
				sum70 -= nbs70[index];
				sum70 += pondNb70;
				nbs70[index] = pondNb70;
				index++;
				if (index == nbs.length)
					index = 0;
				nbUpdated++;
				if (nbUpdated % 60 == 0)
					System.out.print(".");
			}
			if (nbBatch > 0)
				ps.executeBatch();
			ps.close();
//			System.out.println(pChrono.getStringAndRestart());
			synchronized (threadInfo)
			{
				threadInfo.progress += 1.0/11/4;
				threadInfo.status = prefixAge + "Done...";
			}
		}
	}

	public static String computeFullCode(UpdateInfo pUpdateInfo)
	{
		return pUpdateInfo.dpt + (pUpdateInfo.code == -1 ? "000" : String.format("%03d", pUpdateInfo.code));
	}
}
