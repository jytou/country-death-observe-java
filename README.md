The aim of this project is to analyze mortality curves. This project goes along with the PHP project https://gitlab.com/jytou/country-death-observe-php

# Requirements

- A database, preferably mysql, and preferably on a SSD for good performance, with at least 20Gb available.
- Java 8, preferably with eclipse as an IDE (otherwise you'll have to compile yourself with your own IDE).

# Setup

Download files containing death records. For France, those can be obtained here: https://www.data.gouv.fr/en/datasets/fichier-des-personnes-decedees/

Create a database schema to hold the data. Create the tables using the .sql files in jyt.mortality.fr/res

You may want to adapt the sql files (typically the createDateTable.sql file) if you plan to insert data before 2000.

For every downloaded file containing death records, run the following:

```
java -cp . jyt.mortality.fr.FeedMortalityData <dburl> <dbuser> <dbpass> <filename> [1]
```

If the last parameter is omitted, then any duplicates from the files will be filtered and not inserted in the database, in order to avoid having the same person (same name, same date and place of birth) twice (or more) in the database. If this parameter is present, then the program will insert the data "as-is", but you will need to delete the unique key on the "obs" table.

Once this step is done, the raw data has been loaded but we now need to create aggregated statistics on this data for better performance on the website. Run the following:

```
java -cp . jyt.mortality.fr.MortalityUpdateStats <dburl> <dbuser> <dbpass>
```

This will create new tables and populate them with statistical data collected from the obs table. Note that every time you run this, you should optimize those tables later to make sure they don't occupy too much space on your hard disk.

This will open a window to monitor the progress of the update, which runs 10 threads in parallel for faster performance.

After this step succeeds, you are ready to install the website in the sister project https://gitlab.com/jytou/country-death-observe-php
